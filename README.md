# README #

Welcome to the Connection Library. At this point it is still very much in development, however any comments, feedback or contributions would be much appreciated. 

Library written with reference and much gratitude to 'Beej's Guide to Network Programming' by Brian "Beej Jorgensen" Hall.

### Installation ###
Run ./install in the root directory
To force the Debug or Release version (default is Release) run ./install Debug or ./install Release
Libraries are install into /usr/local/lib 
In order to specify another location, the install script must be modified

### Overview ###

* The library exists to allow communication between hosts on a network without having to focus on sockets
* Version 0.1

### Contribution guidelines ###

Anyone interested in contributing please contact Chris Baish, details below

### Developer Contact Details ###

Lead Developer: Chris Baish (chris.baish@gmail.com) - RED Scientific Ltd
