/*
 * Connection.cpp Main source file for the Connection library
 * Copyright (C) 2017 C Baish, RED Scientific Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Connection.h"

namespace Connection_namespace
{

Connection::Connection(char* rx_port, char* tx_port, char* their_addr,
		int flags)
{
	_rx_port = rx_port;
	_tx_port = tx_port;
	_their_addr = their_addr;
	tx_fd = 0;
	rx_fd = 0;
	servinfo_tx = NULL;
	tx_p = NULL;
	_broadcast = ((flags & CONNECTION_BROADCAST) != 0);

	if ((flags & CONNECTION_TX) != 0)
	{
		this->~Connection();
		throw std::invalid_argument("Error: Do not set the Tx or Rx Flags");
		return;
	}

	bool ret = false;
	try
	{
		int rx_flags = (flags & ~CONNECTION_BROADCAST);
		ret = open_socket(NULL, _rx_port, rx_flags);
	}
	catch (std::runtime_error& e)
	{
		this->~Connection();
		throw;
		return;

	}
	if (ret == false)
	{
		this->~Connection();
		throw std::runtime_error("Failed to open rx, unspecified error");
		return;
	}
	try
	{
		int tx_flags = flags | CONNECTION_TX;
		ret = open_socket(_their_addr, _tx_port, tx_flags);
	}
	catch (std::runtime_error& e)
	{
		this->~Connection();
		throw;
		return;
	}
	if (ret == false)
	{
		this->~Connection();
		throw std::runtime_error("Failed to open tx, unspecified error");
		return;
	}
}

Connection::~Connection()
{
	if (servinfo_tx != NULL)
	{
		freeaddrinfo(servinfo_tx);
	}
	if (tx_fd > 0)
	{
		close(tx_fd);
	}
	if (rx_fd > 0)
	{
		close(rx_fd);
	}
}

bool Connection::open_socket(char* address, char* port, int flags)
{
	bool tcp = ((flags & CONNECTION_TCP) != 0);
	/*
	 * Assumption is made that the options are only TCP or UDP.
	 * If more options are added/required this needs changing to an enum.
	 */
	if (tcp)
	{
		throw std::runtime_error(
				"Error: TCP sockets not supported at this time");
		// TODO Add support for TCP sockets
		return false;
	}
	bool tx = ((flags & CONNECTION_TX) != 0);
	bool broadcast = ((flags & CONNECTION_BROADCAST) != 0);

	int rv, fd;
	struct addrinfo hints, *servinfo, *p;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = tx ? 0 : AI_PASSIVE; // use my IP
	char* addr = tx ? address : NULL;

	if ((rv = getaddrinfo(addr, port, &hints, &servinfo)) != 0)
	{
		throw std::runtime_error(
				"getaddrinfo: " + (std::string const) gai_strerror(rv));
		return false;
	}

	for (p = servinfo; p != NULL; p = p->ai_next)
	{
		if ((fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
		{
			perror("Socket");
			continue;
		}

		if (broadcast && tx && !tcp)
		{
			int yes = 1;
			if (setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes))
					== -1)
			{
				perror("setsockopt (SO_BROADCAST)");
				throw std::runtime_error("Failed to set broadcast");
				return false;
			}
		}

		if (!tx)
		{
			if (bind(fd, p->ai_addr, p->ai_addrlen) == -1)
			{
				close(fd);
				perror("Failed to bind");
				continue;
			}
		}

		break;
	}

	if (p == NULL)
	{
		if (tx)
		{
			throw std::runtime_error(
					("Failed to create tx socket for port "
							+ (std::string const) port + " to address "
							+ (std::string const) addr));
		}
		else
		{
			throw std::runtime_error(
					("Failed to create rx socket for port "
							+ (std::string const) port));
		}
		return false;
	}
	if (tx)
	{
		tx_fd = fd;
		tx_p = p;
		servinfo_tx = servinfo;

	}
	else
	{
		rx_fd = fd;
		freeaddrinfo(servinfo);
	}
	return true;
}

bool Connection::send_packet(std::vector<unsigned char> buffer, unsigned int length)
{
#ifdef CONNECTION_PACKET_LOGGING
#ifdef CONNECTION_BUFFER_LOGGING
	std::cout << "In send_packet, port " << _tx_port << " destination ";
	for (unsigned int i = 0; i < length; i++)
	{
		std::cout << tx_p->ai_addr->sa_data[i];
	}
	std::cout << std::endl << "In hex: " << std::hex;
	for (unsigned int i = 0; i < length; i++)
	{
		std::cout << "0x" << (int) (uint8_t) tx_p->ai_addr->sa_data[i] << " ";
	}
	std::cout << std::dec << std::endl;
#endif //CONNECTION_BUFFER_LOGGING
	unsigned int length_str = length;
#endif //CONNECTION_PACKET_LOGGING

	if (buffer.max_size() < length)
	{
		throw std::runtime_error("Buffer not big enough");
		return false;
	}
	long int amount_sent = 0;
	bool error = false;
	int buffer_offset = 0;

	while (length > 0)
	{
		amount_sent = sendto(tx_fd, buffer.data() + buffer_offset, length, 0, tx_p->ai_addr,
				tx_p->ai_addrlen);
		if (amount_sent == 0)
		{
			CONNECTION_LOG("Socket Closed");
			throw std::runtime_error("Socket Closed by other end");
			return false;
		}
		if (amount_sent == -1)
		{
			if (!error)
			{
				error = true;
				CONNECTION_LOG("Single Error");
				amount_sent = 0;
			}
			else
			{
				CONNECTION_LOG("Multiple Consecutive Errors");
				throw std::runtime_error("Multiple Consecutive Transmission Errors");
				return false;
			}
		}
		else
		{
			error = false;
		}
		buffer_offset += (int) amount_sent;
		length -= (int) amount_sent;
	}

#ifdef CONNECTION_PACKET_LOGGING
	CONNECTION_PACKET_LOG(
			"Successfully sent a buffer at " << &buffer << ", length " << length_str);
#ifdef CONNECTION_BUFFER_LOGGING
	std::cout << "Buffer:";
	for (unsigned int i = 0; i < length_str; i++)
	{
		std::cout << buffer[i];
	}
	std::cout << std::endl << "Buffer in hex: " << std::hex;
	for (unsigned int i = 0; i < length_str; i++)
	{
		std::cout << "0x" << (int) (uint8_t) buffer[i] << " ";
	}
	std::cout << std::dec << std::endl;
#endif //CONNECTION_BUFFER_LOGGING
#endif //CONNECTION_PACKET_LOGGING

	return true;
}

bool Connection::broadcast_packet(std::vector<unsigned char> buffer, unsigned int length)
{
	if (!_broadcast)
	{
		CONNECTION_LOG("Error - Broadcast not enabled");
		throw std::runtime_error("Error - Broadcast not enabled. Pass the broadcast flag when initialising the connection");
		return false;
	}

	sockaddr *broadcast_address = new sockaddr();
	socklen_t broadcast_address_length = tx_p->ai_addrlen;
	memcpy(broadcast_address, tx_p->ai_addr, broadcast_address_length);
	broadcast_address->sa_data[5] = '\0';

#ifdef CONNECTION_PACKET_LOGGING
#ifdef CONNECTION_BUFFER_LOGGING
	std::cout << "In broadcast_packet, port " << _tx_port << " destination ";
	for (unsigned int i = 0; i < length; i++)
	{
		std::cout << broadcast_address->sa_data[i];
	}
	std::cout << std::endl << "In hex: " << std::hex;
	for (unsigned int i = 0; i < length; i++)
	{
		std::cout << "0x" << (int) (uint8_t) broadcast_address->sa_data[i] << " ";
	}
	std::cout << std::dec << std::endl;
#endif //CONNECTION_BUFFER_LOGGING
	unsigned int length_str = length;
#endif //CONNECTION_PACKET_LOGGING

	if (buffer.max_size() < length)
	{
		throw std::runtime_error("Buffer not big enough");
		return false;
	}
	long int amount_sent = 0;
	bool error = false;
	int buffer_offset = 0;

	while (length > 0)
	{
		amount_sent = sendto(tx_fd, buffer.data() + buffer_offset, length, 0, broadcast_address,
				broadcast_address_length);
		if (amount_sent == 0)
		{
			CONNECTION_LOG("Socket Closed");
			throw std::runtime_error("Socket Closed by other end");
			return false;
		}
		if (amount_sent == -1)
		{
			if (!error)
			{
				error = true;
				CONNECTION_LOG("Single Error");
				amount_sent = 0;
			}
			else
			{
				CONNECTION_LOG("Multiple Consecutive Errors");
				throw std::runtime_error("Multiple Consecutive Transmission Errors");
				return false;
			}
		}
		else
		{
			error = false;
		}
		buffer_offset += (int) amount_sent;
		length -= (int) amount_sent;
	}

#ifdef CONNECTION_PACKET_LOGGING
	CONNECTION_PACKET_LOG(
			"Successfully broadcasted a buffer at " << &buffer << ", length " << length_str);
#ifdef CONNECTION_BUFFER_LOGGING
	std::cout << "Buffer:";
	for (unsigned int i = 0; i < length_str; i++)
	{
		std::cout << buffer[i];
	}
	std::cout << std::endl << "Buffer in hex: " << std::hex;
	for (unsigned int i = 0; i < length_str; i++)
	{
		std::cout << "0x" << (int) (uint8_t) buffer[i] << " ";
	}
	std::cout << std::dec << std::endl;
#endif //CONNECTION_BUFFER_LOGGING
#endif //CONNECTION_PACKET_LOGGING

	return true;
}

bool Connection::recv_packet(std::vector<unsigned char>& buffer, unsigned int length)
{
	CONNECTION_PACKET_LOG("In recv_packet, port " << _rx_port)
#ifdef CONNECTION_PACKET_LOGGING
	unsigned int length_str = length;
#endif //CONNECTION_PACKET_LOGGING

	long int amount_received = 0;
	bool error = false;
	int buffer_offset = 0;

	if (buffer.max_size() < length)
	{
		throw std::runtime_error("Buffer not big enough");
		return false;
	}

	while (length > 0)
	{
		amount_received = recvfrom(rx_fd, buffer.data() + buffer_offset, length, 0,
				NULL, NULL);
#ifdef CONNECTION_PACKET_LOGGING
		int myerrno = errno;
		CONNECTION_PACKET_LOG("Received " << amount_received << ", Error type: " << strerror(myerrno));
#endif //CONNECTION_PACKET_LOGGING
		if (amount_received == 0)
		{
			CONNECTION_LOG("Error: Socket closed");
			return false;
		}
		else if (amount_received == -1)
		{
			if (!error)
			{
				error = true;
				CONNECTION_LOG("Single Error");
				amount_received = 0;
			}
			else
			{
				CONNECTION_LOG("Multiple Consecutive Errors");
				throw std::runtime_error("Multiple Consecutive Receiving Errors");
				return false;
			}
		}
		else
		{
			error = false;
		}
		buffer_offset += (int) amount_received;
		length -= (int) amount_received;
	}
#ifdef CONNECTION_PACKET_LOGGING
	CONNECTION_PACKET_LOG(
			"Successfully received a buffer at " << &buffer << ", length " << length_str);
#ifdef CONNECTION_BUFFER_LOGGING
	std::cout << "Buffer:";
	for (unsigned int i = 0; i < length_str; i++)
	{
		std::cout << buffer[i];
	}
	std::cout << std::endl << "Buffer in hex: " << std::hex;
	for (unsigned int i = 0; i < length_str; i++)
	{
		std::cout << "0x" << (int) (uint8_t) buffer[i] << " ";
	}
	std::cout << std::dec << std::endl;
#endif //CONNECTION_BUFFER_LOGGING
#endif //CONNECTION_PACKET_LOGGING
	return true;
}

void *Connection::get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET)
	{
		return &(((struct sockaddr_in*) sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*) sa)->sin6_addr);
}

} /* namespace Connection_namespace */
