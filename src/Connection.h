/*
 * Connection.h Header file for the Connection library
 * Copyright (C) 2017 C Baish, RED Scientific Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

// C++ Library Headers
#include <iostream>
#include <exception>
#include <vector>

// C Library Headers
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

namespace Connection_namespace
{

// Own Headers
#include "Connection_macros.h"

class Connection
{
public:
	Connection(char* rx_port, char* tx_port, char* their_addr, int flags = CONNECTION_NO_FLAGS);
	virtual ~Connection();
	bool send_packet(std::vector<unsigned char> buffer, unsigned int length);
	bool recv_packet(std::vector<unsigned char>& buffer, unsigned int length);
	bool broadcast_packet(std::vector<unsigned char> buffer, unsigned int length);
private:
	bool open_socket(char* address, char* port, int flags);
	void *get_in_addr(struct sockaddr *sa);
	struct addrinfo *tx_p, *servinfo_tx;
	int tx_fd, rx_fd;
	char *_rx_port, *_tx_port, *_their_addr;
	bool _broadcast;
};

} /* namespace Connection_namespace */

#endif /* CONNECTION_H_ */
