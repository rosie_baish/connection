/*
 * Connection_Test.cpp Test suite file for the Connection library
 * Copyright (C) 2017 C Baish, RED Scientific Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Connection test

// Boost headers
#include <boost/test/unit_test.hpp>

// C Headers
#include <pthread.h>

// C++ Headers
#include <vector>
#include <iostream>

// My Headers
#include "Connection.h"

#ifdef ALL_LOGGING
#define BOOST_TEST_LOG_LEVEL all
#endif // ALL_LOGGING

BOOST_AUTO_TEST_CASE( constructor_test )
{
#define TEST_RX_PORT (char*)"3492"
#define TEST_TX_PORT (char*)"3493"
#define TEST_THEIR_ADDR (char*)"127.0.0.1"
	int max_flag = (CONNECTION_TX | CONNECTION_BROADCAST | CONNECTION_TCP);
	for (int i = 0; i < max_flag; i++)
	{
		if ((i & CONNECTION_TX) != 0)
		{
			BOOST_CHECK_THROW(
					Connection_namespace::Connection connection1(TEST_RX_PORT, TEST_TX_PORT, TEST_THEIR_ADDR, i),
					std::invalid_argument);
		}
		else if ((i & CONNECTION_TX) != 0)
		{
			BOOST_CHECK_THROW(
					Connection_namespace::Connection connection1(TEST_RX_PORT, TEST_TX_PORT, TEST_THEIR_ADDR, i),
					std::runtime_error);
		}

	}
#undef TEST_RX_PORT
#undef TEST_TX_PORT
#undef TEST_THEIR_ADDR
}

bool server_initialised;

void *test_server(void*)
{
	std::cout << "In test_server" << std::endl;
#define TEST_RX_PORT (char*)"3492"
#define TEST_TX_PORT (char*)"3493"
#define TEST_THEIR_ADDR (char*)"127.0.0.1"
	Connection_namespace::Connection server(TEST_TX_PORT, TEST_RX_PORT,
	TEST_THEIR_ADDR, 0);
	std::vector<unsigned char> buffer(256, '\0');
	std::cout << "In test_server, waiting to receive" << std::endl;
	server_initialised = true;
	server.recv_packet(buffer, 256);
	std::cout << "In test_server, waiting to send" << std::endl;
	server.send_packet(buffer, 256);
	std::cout << "In test_server, finished sending" << std::endl;
#undef TEST_RX_PORT
#undef TEST_TX_PORT
#undef TEST_THEIR_ADDR
	std::cout << "Leaving test_server" << std::endl;
	pthread_exit(NULL);
}

BOOST_AUTO_TEST_CASE( transmission_test )
{
#define TEST_RX_PORT (char*)"3492"
#define TEST_TX_PORT (char*)"3493"
#define TEST_THEIR_ADDR (char*)"127.0.0.1"
	server_initialised = false;
	pthread_t thread;
	pthread_create(&thread, NULL, test_server, NULL);
	unsigned int length = 256;
	std::vector<unsigned char> tx_buffer;
	for (unsigned int i = 0; i < length; i++)
	{
		tx_buffer.push_back((unsigned char) i);
	}
	std::vector<unsigned char> rx_buffer(length, '\0');
	std::cout << "Waiting for server to be initialised" << std::endl;
	while (!server_initialised)
	{
		;
	}
	std::cout << "Server has been initialised" << std::endl;
	Connection_namespace::Connection connection_tx(TEST_RX_PORT, TEST_TX_PORT,
	TEST_THEIR_ADDR, 0);
	std::cout << "Created test case" << std::endl;

	connection_tx.send_packet(tx_buffer, length);
	std::cout << "Sent, Waiting to receive" << std::endl;
	connection_tx.recv_packet(rx_buffer, length);
	std::cout << "Received" << std::endl;

	BOOST_CHECK_EQUAL_COLLECTIONS(tx_buffer.begin(), tx_buffer.end(),
			rx_buffer.begin(), rx_buffer.end());
	std::cout << "Checking equal" << std::endl;

#undef TEST_RX_PORT
#undef TEST_TX_PORT
#undef TEST_THEIR_ADDR
}


BOOST_AUTO_TEST_CASE( broadcast_test )
{
#define TEST_RX_PORT (char*)"3492"
#define TEST_TX_PORT (char*)"3493"
#define TEST_THEIR_ADDR (char*)"127.0.0.1"
	server_initialised = false;
	pthread_t thread;
	pthread_create(&thread, NULL, test_server, NULL);
	unsigned int length = 256;
	std::vector<unsigned char> tx_buffer;
	for (unsigned int i = 0; i < length; i++)
	{
		tx_buffer.push_back((unsigned char) (length - i - 1));	// Reverse the order to make it more visually destinctive
	}
	std::vector<unsigned char> rx_buffer(length, '\0');
	std::cout << "Waiting for server to be initialised" << std::endl;
	while (!server_initialised)
	{
		;
	}
	std::cout << "Server has been initialised" << std::endl;
	int flags = CONNECTION_BROADCAST;
	Connection_namespace::Connection connection_tx(TEST_RX_PORT, TEST_TX_PORT,
	TEST_THEIR_ADDR, flags);
	std::cout << "Created test case" << std::endl;

	std::cout << "About to Broadcast" << std::endl;
	connection_tx.broadcast_packet(tx_buffer, length);
	std::cout << "Broadcasted, Waiting to receive" << std::endl;
	connection_tx.recv_packet(rx_buffer, length);
	std::cout << "Received" << std::endl;

	BOOST_CHECK_EQUAL_COLLECTIONS(tx_buffer.begin(), tx_buffer.end(),
			rx_buffer.begin(), rx_buffer.end());
	std::cout << "Checking equal" << std::endl;

#undef TEST_RX_PORT
#undef TEST_TX_PORT
#undef TEST_THEIR_ADDR
}
