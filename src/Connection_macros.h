/*
 * Connection_macros.h Additional header file for the Connection library, containing Macros
 * Copyright (C) 2017 C Baish, RED Scientific Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_MACROS_H_
#define CONNECTION_MACROS_H_

namespace Connection_namespace
{

#define CONNECTION_NO_FLAGS		0x00
#define CONNECTION_RX			0x00
#define CONNECTION_UDP			0x00
#define CONNECTION_TX			0x01
#define CONNECTION_BROADCAST	0x02
#define CONNECTION_TCP			0x04

/*
 * Logging Levels. Each one can be manually turned on and off here.
 * The #ifdef ALL_LOGGING below automatically turns them all on
 * The #ifdef NO_LOGGING below automatically turns them all off (over-rides ALL_LOGGING)
 * New logging levels need to be added to both of those sections, and given their own #define here
 */

// #define CONNECTION_LOGGING
// #define CONNECTION_PACKET_LOGGING
// #define CONNECTION_BUFFER_LOGGING
// #define ALL_LOGGING
// #define NO_LOGGING


#ifdef ALL_LOGGING
#define CONNECTION_BUFFER_LOGGING
#define CONNECTION_LOGGING
#define CONNECTION_PACKET_LOGGING
#endif // ALL_LOGGING

#ifdef NO_LOGGING
#undef ALL_LOGGING
#undef CONNECTION_BUFFER_LOGGING
#undef CONNECTION_LOGGING
#undef CONNECTION_PACKET_LOGGING
#endif // NO_LOGGING

#ifdef CONNECTION_PACKET_LOGGING
#define CONNECTION_PACKET_LOG(msg) \
	std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl;
#else // CONNECTION_PACKET_LOGGING
#define CONNECTION_PACKET_LOG(msg) ;
#endif // CONNECTION_PACKET_LOGGING

#ifdef CONNECTION_LOGGING
#define CONNECTION_LOG(msg) \
		std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl;
#else // CONNECTION_LOGGING
#define CONNECTION_LOG(msg) ;
#endif // CONNECTION_LOGGING

} // namespace Connection_namespace

#endif /* CONNECTION_MACROS_H_ */
